from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, EmailField, DateTimeLocalField, SelectField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from .models import User, Status


class LoginForm(FlaskForm):
    username = StringField('Имя пользователя', validators=[DataRequired()])
    password = PasswordField('Пароль', validators=[DataRequired()])
    remember_me = BooleanField('Запомни меня')
    submit = SubmitField('Войти')


class TaskForm(FlaskForm):
    title = StringField('Задача', validators=[DataRequired(), Length(min=3, max=200)])
    deadline = DateTimeLocalField('Срок выполнения', format='%Y-%m-%dT%H:%M')
    submit = SubmitField('Создать')


class TaskUpdateForm(FlaskForm):
    title = StringField('Задача', validators=[DataRequired(), Length(min=3, max=200)])
    deadline = DateTimeLocalField('Срок выполнения', format='%Y-%m-%dT%H:%M')
    status = SelectField('Статус', coerce=int)
    submit = SubmitField('Создать')

    def __init__(self, *args, **kwargs):
        super(TaskUpdateForm, self).__init__(*args, **kwargs)
        self.status.choices = [(status.id, status.name) for status in Status.query.all()]


class RegisterForm(FlaskForm):
    username = StringField('Имя пользователя', validators=[DataRequired()])
    email = EmailField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Пароль', validators=[DataRequired()])
    password2 = PasswordField('Подтвердите пароль', validators=[DataRequired(), EqualTo('password')])

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('Пользователь с таким именем уже существует!')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError("Пользователь с таким email'ом уже существует!")
