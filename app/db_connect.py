import psycopg2


def connect_db():
    conn = psycopg2.connect(
        host="localhost",
        user="postgres",
        password="postgres",
        database="todolist_db"
    )
    return conn


conn = connect_db()
cur = conn.cursor()
cur.execute('select * from task;')
tasks = cur.fetchall()
print(tasks)
cur.close()
conn.close()
