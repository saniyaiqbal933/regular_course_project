from flask import render_template, flash, redirect, url_for, request
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import urlsplit

from app import app, db
from .forms import LoginForm, TaskForm, RegisterForm, TaskUpdateForm
from .models import User, Task, Status


@app.route('/', methods=['GET'])
def index():
    tasks = Task.query.all()
    return render_template('index.html', title='Главная', tasks=tasks)


@app.route('/tasks/create', methods=['GET', 'POST'])
@login_required
def task_create():
    form = TaskForm()
    if form.validate_on_submit():
        task = Task(
            title=form.title.data,
            author=current_user,
            status=Status.query.filter_by(name='Новая').first(),
            deadline=form.deadline.data
        )
        db.session.add(task)
        db.session.commit()
        flash('Your task has been created!')
        return redirect(url_for('task_detail', task_id=task.id))
    return render_template('task_create.html', title='Создание задачи', form=form)


@app.route('/tasks/<int:task_id>', methods=['GET'])
def task_detail(task_id):
    task = Task.query.get_or_404(task_id)
    return render_template('task_detail.html', title='Просмотр задачи', task=task)


@app.route('/tasks/<int:task_id>/update', methods=['GET', 'POST'])
@login_required
def task_update(task_id):
    task = Task.query.get_or_404(task_id)
    form = TaskUpdateForm()
    if form.validate_on_submit():
        task.title = form.title.data
        task.deadline = form.deadline.data
        task.status_id = form.status.data
        db.session.commit()
        flash('Your task has been updated!')
        return redirect(url_for('task_detail', task_id=task_id))
    elif request.method == 'GET':
        form.title.data = task.title
        form.deadline.data = task.deadline
        form.status.data = task.status_id
    return render_template('task_update.html', title='Редактирование задачи', form=form, task=task)


@app.route('/tasks/<int:task_id>/delete', methods=['GET', 'POST'])
@login_required
def task_delete(task_id):
    task = Task.query.get_or_404(task_id)
    if request.method == 'POST':
        db.session.delete(task)
        db.session.commit()
        flash('Your task has been deleted!')
        return redirect(url_for('index'))
    return render_template('task_delete.html', title='Удаление задачи', task=task)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or urlsplit(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Вход', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegisterForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Пользователь зарегистрирован')
        return redirect(url_for('login'))
    return render_template('register.html', title='Регистрация', form=form)

